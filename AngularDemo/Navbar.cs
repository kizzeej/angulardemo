﻿using System;
using System.Web.SessionState;

namespace AngularDemo
{
    public class Navbar
    {
        public static string BuildNavbar(HttpSessionState session)
        {
            string navbar = "<li ng-class=\"{ active: isActive('/landing')}\"><a href=\"app.aspx#!/landing\" class=\"nav-link\">Landing</a></li>";
            navbar += "<li ng-class=\"{active : isActive('/data')}\"><a href=\"app.aspx#!/data\" class=\"nav-link\">Data</a></li>";

            return navbar;
        }
    }
}