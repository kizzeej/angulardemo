﻿app.factory('dataapi', function ($resource) {
    return $resource('api/v1/data/dataapi.ashx', { now: '@now' }, { save: { method: 'POST', isArray: true }, update: { method: 'PUT', isArray: true }, delete: { method: 'DELETE', isArray: true } });
});
