﻿DataCtrl = function ($scope, dataapi) {
    //Define some scope variables to be used in the JS and DOM
    $scope.inputdata = {};  //This will be bound to the input fields
    $scope.data = [];

    //Used to clear the input boxes and ng-model 'inputdata'
    $scope.clearinput = function () {
        $scope.inputdata = {};
    }

    //Used to select a 'cloned' version of a selected data item in the table for editing
    //Cloning into a new object breaks the 2-way binding so the table is not updated when we edit the input fields
    $scope.selectassetassignment = function (assignmentdata) {       
        //We use this method instead of direct inputdata = assignmentdata in order to break the 2-Way binding so the table item is not updated until we save the changes
        $scope.inputdata = {
            uid: assignmentdata.uid,
            name: assignmentdata.name,
            assetnumber: assignmentdata.assetnumber,
            assigneddate: new Date(assignmentdata.assigneddate)
        }
    }

    //Setup a function for requesting data
    $scope.getdata = function () {
        //Use the 'dataapi' resource defined in 'appresources.js' that links to the 'api/v1/data/dataapi.ashx' handler
        dataapi.query({ now: new Date().getTime() }, function (rtndata) {   //rtndata is the return data from the api in JSON format. We use newtonsoft json .net to package a list<dictionary<string, object>> from the SQL server as data rows
            //Use the 'checkerrors' function from 'app.js' to check for errors in the rtndata response from the api.  These could be SQL server faults or built in error responses from stored procedures
            if (checkerrors(rtndata)) {
                return;
            }

            //If no errors in the rtndata response then define a scope variable equal to rtndata so we can use it on the DOM
            $scope.data = rtndata;

            //Ultimately $scope.data will be used in a ng-repeat statement to build a table
            //2-way binding immediately updates the DOM when the variable is assigned. So everything is in sync
        });

         

        $scope.clearinput();
    }

    //Initialize data request as page loads so we have data to display. 
    $scope.getdata();

    $scope.savedata = function () {
        //Create a local variable to store only the parts of the inputdata variable we want.
        //Without this the inputvariable contains several angular specific properties that come along for the ride.
        //However, 'inputdata' could be used directly in the save statement below
        var datatosave = {
            uid: ($scope.inputdata.uid ? $scope.inputdata.uid : null),
            name: $scope.inputdata.name,
            assetnumber: $scope.inputdata.assetnumber,
            assigneddate: $scope.inputdata.assigneddate
        }

        //If the inputdata.uid is not null then we need to use 'PUT' to update the database
        //Typically I just run POST and check on the back end if the UID exists or not and save/update accordingly
        if ($scope.inputdata.uid) {
            dataapi.update(datatosave, function (rtndata) {
                //Check for errors in rtndata and display a pop up to the user is any errors happen
                if (checkerrors(rtndata)) {
                    return;
                }

                //Notify user that the action was successful
                notify('Asset assignment updated successfully!', 'fa-check', 'success');

                //Typicaly here we would pull the latest list using getdata 
                //This pull would also update the DOM because of Angular's 2-Way binding
                //$scope.getdata();

                //For demo we just update the table item....
                angular.forEach($scope.data, function (item, itemkey) {
                    if (item.uid == $scope.inputdata.uid) {
                        item.name = $scope.inputdata.name;
                        item.assetnumber = $scope.inputdata.assetnumber;
                        item.assigneddate = $scope.inputdata.assigneddate;
                    }
                });

                //Clear the input
                $scope.inputdata = {};
            })
        }
        else {
            //Save the data 'POST' and then read the response from the API
            dataapi.save(datatosave, function (rtndata) {
                //Check for errors in rtndata and display a pop up to the user is any errors happen
                if (checkerrors(rtndata)) {
                    return;
                }

                //Notify user that the action was successful
                notify('Asset assignment added sucessfully', 'fa-check', 'success');

                //Typicaly here we would pull the latest list using getdata 
                //This pull would also update the DOM because of Angular's 2-Way binding
                //$scope.getdata();

                //For demo we just update the table item
                $scope.inputdata.uid = Math.random * 1000;
                $scope.data.push($scope.inputdata);

                //Clear the input
                $scope.inputdata = {};
            })
        }
    }

    $scope.deleteitem = function (item) {
        dataapi.delete({ uid: item.uid }, function (rtndata) {
            //Check for errors in rtndata and display a pop up to the user is any errors happen
            if (checkerrors(rtndata)) {
                return;
            }

            //Notify user that the action was successful
            notify('Asset assignment deleted sucessfully', 'fa-check', 'success');

            //Typically here we would call on getdata() to pull the latest list after the delete and update the DOM
            //For the purpose of the demo we just remove the item from the data array
            angular.forEach($scope.data, function (dataitem, dataitemkey) {
                if (dataitem.uid == item.uid) {
                    $scope.data.splice(dataitemkey, 1);
                }
            });
        });
    }
}