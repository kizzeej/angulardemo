﻿//Here we define the modules that will be used 
var app = angular.module('angulardemo', ['ngRoute', 'ngResource']).config(function ($sceDelegateProvider) {
    //When accessing a resource that we define we set a whitelist of Urls that can be hit.  
    //Add to this white list if you are using external APIs
    $sceDelegateProvider.resourceUrlWhitelist(['self']);
});

//This sets up the routes for the single page app so we can navigate mulitple views
//Each view needs a HTML template and associated controller (usually placed in its own JS file loaded on the app.aspx page)
app.config(function ($routeProvider, $locationProvider) {
    $routeProvider.
        when('/landing', { controller: LandingCtrl, templateUrl: 'views/landing/landing.html' }).
        when('/data', { controller: DataCtrl, templateUrl: 'views/data/data.html' }).
        otherwise({ redirectTo: '/data' });
});

//This is the section that starts the app and ties all the setup pieces together.
//The controller name 'AppCtrl' is referenced under the 'ng-app' property of the head tag in the DOM
app.controller("AppCtrl", function ($scope, $location) {
    //A function to test which navigation item needs to be active.  In the navbar the 'active' class is triggered by this function for each nav item based on the current URL
    $scope.isActive = function (viewLocation) {
        var test = $location.url();
        var active = ($location.url().indexOf(viewLocation) > -1);
        return active;
    };       
});

//This checks for errors in API responses
function checkerrors(data) {
    test = false;
    if (data.length > 0) {
        if (data[0]["error"]) {
            if (data[0]["error"].indexOf("No Session Token") > -1) {
                window.location.reload();

                return
            }

            notify(data[0]["error"], 'fa-warning', 'danger')
            test = true;
        }
    }
    return test;
}

//This creates a popup to notify users of actions taken or errors
function notify(message, icon, notifytype) {
    $.notify({
        //options
        icon: 'fa ' + icon,
        message: message
    }, {
            element: 'body',
            type: notifytype,
            placement: { from: 'top', align: 'center' },
            delay: (notifytype == 'danger' ? null : 3000),
            timer: 1000,
            url_target: '_blank',
            mouse_over: null,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });
}