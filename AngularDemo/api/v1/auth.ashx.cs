﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;

namespace AngularDemo.api.v1
{
    /// <summary>
    /// Summary description for auth
    /// </summary>
    public class auth : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {            

            if (context.Request.HttpMethod == "GET")
            {
                context.Session["username"] = context.User.Identity.Name.Split('\\')[context.User.Identity.Name.Split('\\').Length - 1];

                context.Response.Redirect("~/" + context.Request.Params["rtnurl"]);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}