﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;

namespace AngularDemo.api.v1.data
{
    /// <summary>
    /// Summary description for auth
    /// </summary>
    public class dataapi: IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {           

            if (context.Request.HttpMethod == "GET")
            {
                List<Dictionary<string, object>> rtndata = new List<Dictionary<string, object>>();

                Dictionary<string, object> dataitem = new Dictionary<string, object>();
                dataitem.Add("uid", 1);
                dataitem.Add("name", "Amazing Data");
                dataitem.Add("assetnumber", 1);
                dataitem.Add("assigneddate", new DateTime(2019,6,10));

                rtndata.Add(dataitem);

                dataitem = new Dictionary<string, object>();
                dataitem.Add("uid", 2);
                dataitem.Add("name", "Simple Data");
                dataitem.Add("assetnumber", 222);
                dataitem.Add("assigneddate", new DateTime(2019, 6, 12));

                rtndata.Add(dataitem);

                dataitem = new Dictionary<string, object>();
                dataitem.Add("uid", 3);
                dataitem.Add("name", "Laptop");
                dataitem.Add("assetnumber", 333);
                dataitem.Add("assigneddate", new DateTime(2019, 6, 16));

                rtndata.Add(dataitem);
                
                context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(rtndata));
            }

            if (context.Request.HttpMethod == "POST")
            {
                //Post Data Here to SQL or other database
            }

            if (context.Request.HttpMethod == "PUT")
            {
                //Update data with an existing UID from here into a database
            }

            if (context.Request.HttpMethod == "DELETE")
            {
                //Delete data here
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}