﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="app.aspx.cs" Inherits="AngularDemo.app" %>

<% if (Session["username"] == null) { Response.Redirect("~/api/v1/auth.ashx?rtnurl=app.aspx"); } %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" ng-app="angulardemo">
<head runat="server">
    
    <title>Audit Tracker</title>
    <link href="assets/framework/css/bootstrap-flatly-4.min.css" rel="stylesheet" />
    <link href="assets/framework/css/bootstrap-toggle.min.css" rel="stylesheet" />
    <link href="assets/framework/css/extras.css" rel="stylesheet" />
    <link href="assets/framework/css/font-awesome.min.css" rel="stylesheet" />    
    <link rel="shortcut icon" href="assets/img/favicon.png" />

    <style>
        .table-border-thick {
            border: 3px solid black;
        }
    </style>

    <%TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
    int secondsSinceEpoch = (int)t.TotalSeconds; %>
    
    <script type="text/javascript" src='assets/framework/js/jquery-3.2.1.min.js'></script>
    <script type="text/javascript" src='assets/framework/js/bootstrap.min.js'></script>
    <script type="text/javascript" src='assets/framework/js/bootstrap-toggle.min.js'></script>    
    <script type="text/javascript" src='assets/framework/js/bootstrap-notify.min.js'></script>
    <script type="text/javascript" src='assets/framework/js/excellentexport.js'></script>
    <script type="text/javascript" src='assets/framework/js/angular.min.js'></script>
    <script type="text/javascript" src='assets/framework/js/angular-resource.min.js'></script>
    <script type="text/javascript" src='assets/framework/js/angular-route.min.js'></script>
    
    <script type="text/javascript" src='views/landing/landing.js'></script>
    <script type="text/javascript" src='views/data/data.js'></script>
   
    <script type="text/javascript" src='app.js'></script>
    <script type="text/javascript" src='appresources.js'></script>
</head>

<body ng-controller="AppCtrl" style="padding-top:80px; margin-bottom:60px; background-color:rgba(50,50,50, .1);">
    
    <div ng-init="username='<%= Session["username"] %>'"></div>      
    
	<div class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
        <div class="container">
            <a href="../" class="navbar-brand"><i class="fa fa-edit"></i> Audit Tracker</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav">
                    <li ng-class="{ active: isActive('/landing')}"><a href="app.aspx#!/landing" class="nav-link">Landing</a></li>
                    <li ng-class="{ active : isActive('/data')}"><a href="app.aspx#!/data" class="nav-link">Data</a></li>               
                </ul>

                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item">
                        <a id="A1" class="nav-link">{{username}}</a>
                    </li>
                </ul>

            </div>
        </div>
    </div>

    <div ng-view></div>	

</body>
</html>