# Angular Demo

- Setting up IIS to handle PUT & DELETE
	- How to disable WebDAV
		- Turn On/Off Windows Features
		- Internet Information Services
		- World Wide Web Services
		- Common HTTP Features
		- Uncheck WebDAV Publishing
		- This is needed because WebDAV will intercept PUT and DELETE requests to IIS and cause a 'Method Not Allowed' error
	- The Web.config file in this build will do the rest of the setup to allow PUT & DELETE on ASHX handlers